import { Component, OnInit, AfterContentInit, Output, EventEmitter, OnDestroy, AfterContentChecked, AfterViewChecked, AfterViewInit } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { Router } from '@angular/router';
import { EnlacesService } from '../shared/enlaces.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'new-enlace',
  templateUrl: './new-enlace.component.html',
  styleUrls: ['./new-enlace.component.css']
})
export class NewEnlaceComponent implements AfterViewInit {
  errorTitulo: boolean;
  errorEnlace: boolean;
  errorEnvio: boolean;
  isLoading: boolean;
  private url: URL;
  urlProtocol: string;
  urlSource: string;
  titulo: string;
  linkCorto: string;

  constructor(private router: Router, private api: ApiService, private enlaces: EnlacesService) {}

  ngAfterViewInit(): void {
    // inittialize all modals
    $('.coupled.modal').modal({ allowMultiple: false});

    $('.second.modal')
    .modal({
      context: $('#ContainerModal2'),
      closable: false,
      autofocus: false,
      transition: 'fade up',
      onVisible: () => {
        this.accept();
      },
      onHide: () => {
        this.enlaces.update();
        this.router.navigate(['/main/enlaces']);
      }
    })
    // open second modal on first modal ok button
    .modal('attach events', '#next');
    // show first modal

    $('.first.modal').modal({
      context: $('#ContainerModal1'),
      closable: false,
      autofocus: false,
      transition: 'fade up',
      onDeny: () => {
        this.router.navigate(['/main/enlaces']);
      },
      onApprove: () => {
        this.next();
      }
    }).modal('show');

    $('.ui.dropdown.label').dropdown();
  }

  pasteUrl(e) {
    try {
      setTimeout(() => { // Sin el delay pega dos veces
        this.url = new URL(e);
        console.log(this.url);
        this.urlSource = this.url.host + this.url.pathname;
        this.urlProtocol = this.url.protocol;
        ($('#inputUrlProtocol') as unknown as HTMLDivElement)[0].innerText = this.url.protocol;
      }, 1);
    } catch {
      this.urlProtocol = 'https://';
      this.urlSource = e;
    }
  }

  copy() {
    ($('#shortLink').get(0) as HTMLInputElement).select();
    document.execCommand('copy');
    $('#shortLink').next().get(0).childNodes[1].nodeValue = 'Copied';
  }

  trashButton() {
    this.urlSource = '';
    this.urlProtocol = 'https://';
    ($('#inputUrlProtocol') as unknown as HTMLDivElement)[0].innerText = 'https://';
  }

  next() {
    if (!this.url) { // Si no se ha pegado la url, se genera
      this.urlProtocol = ($('#inputUrlProtocol') as unknown as HTMLDivElement)[0].innerText;
      this.url = new URL(this.urlProtocol + this.urlSource);
    }
  }

  accept() {
    console.log('saving');
    this.isLoading = true;
    this.errorEnvio = false;
    this.api.createEnlace(this.titulo, this.url.href).subscribe(
      res => {
        this.isLoading = false;
        $('.positive.button').get(0).childNodes.item(0).textContent = 'Guardado';
        $('.negative.button').get(0).innerText = 'Cerrar';
        this.linkCorto = res.data.shortLink;
        $('#oculto').css('display', 'initial');
      }, err => {
        this.isLoading = false;
        this.errorEnvio = true;
      }
    );
  }
}
