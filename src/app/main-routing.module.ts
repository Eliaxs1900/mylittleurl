import { Routes } from '@angular/router';
import { EnlacesComponent } from './enlaces/enlaces.component';
import { ConfigPageComponent } from './config-page/config-page.component';
import { NewEnlaceComponent } from './new-enlace/new-enlace.component';
import { ShowDataEnlaceComponent } from './show-enlace/show-enlace.component';

export const routes: Routes = [
    { path: '', redirectTo: 'enlaces', pathMatch: 'full' },
    { path: 'enlaces', component: EnlacesComponent },
    { path: 'enlaces/new', component: NewEnlaceComponent},
    { path: 'enlaces/info/:id', component: ShowDataEnlaceComponent },
    { path: 'config', component: ConfigPageComponent }
];
