import { Component, HostListener, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { screenSize, orderType } from '../shared/global';
import { EnlacesService } from '../shared/enlaces.service';
import { UserService } from '../shared/user.service';
import { Subscription } from 'rxjs';
import { UserModel } from '../shared/models/user.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  mostrarInformacionEnlace = false;
  mostrarimagenAvatar = false;
  isDownSortedDirection: boolean;
  imagenAvatar: any;
  usuario: UserModel;
  isDevice: string;

  constructor(private router: Router, private user: UserService, private enlacesServ: EnlacesService) {
    jQuery(() => { $('.dropdown').dropdown(); });
    this.isDownSortedDirection = true;
  }

  ngOnInit(): void {
    this.onResize();
    this.subscription = this.user.imagen$.subscribe((imagenChanged) => {
      this.imagenAvatar = imagenChanged;
    });
    this.subscription = this.user.user$.subscribe((datosCambiados) => {
      this.usuario = datosCambiados;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ModalImagenAvatar(): void { this.mostrarimagenAvatar = !this.mostrarimagenAvatar; }
  ModalInfoacionEnlace(): void { this.mostrarInformacionEnlace = !this.mostrarInformacionEnlace; }


  // FUNCIONAL 💖

  toggleDirection() {
    this.enlacesServ.isSortDescendent(this.isDownSortedDirection);
    this.isDownSortedDirection = !this.isDownSortedDirection;
  }

  byImpact() {
    this.enlacesServ.setOrder(orderType.Impact);
  }

  byTime() {
    this.enlacesServ.setOrder(orderType.Time);
  }

  byTitle() {
    this.enlacesServ.setOrder(orderType.Title);
  }

  filter() {
    const texto = ($('#buscador input').get(0)) as HTMLInputElement;
    this.enlacesServ.filter(texto.value);
  }

  update() {
    this.enlacesServ.update();
  }


  @HostListener('window:resize')
  onResize() {
    const size = window.innerWidth;

    if (size < 768) {
      this.isDevice = screenSize.MOBILE;
    } else if (size >= 768 && size <= 991) {
      this.isDevice = screenSize.TABLET;
    } else if (size >= 992 && size <= 1200) {
      this.isDevice = screenSize.SMALL_MONITOR;
    } else {
      this.isDevice = screenSize.LARGE_MONITOR;
    }
    jQuery(() => { $('.dropdown').dropdown(); });
  }

  sidebar() {
    $('.sidebar')
    .sidebar({
      context: $('#containerMenu'),
      transition: 'push',
      defaultTransition: { mobile: { left: 'overlay' }}
    })
    .sidebar('toggle');
  }

  logout() {
    this.user.removeUser();
    this.router.navigate(['/login']);
  }
}
