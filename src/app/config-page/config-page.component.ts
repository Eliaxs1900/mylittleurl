import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { UserService } from '../shared/user.service';
import { EnlacesService } from '../shared/enlaces.service';
import { Card } from '../shared/models/card.model';
import { UserModel } from '../shared/models/user.model';
import { UserTypes } from '../shared/global';
import { ApiService } from '../shared/api.service';
import { Subscription } from 'rxjs';

const months = [
  'Enero', 'Febrero', 'Marzo', 'Abril',
  'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre',
  'Octubre', 'Noviembre', 'Diciembre'
];

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'config',
  templateUrl: './config-page.component.html',
  styleUrls: ['./config-page.component.css']
})
export class ConfigPageComponent implements AfterViewInit, OnDestroy {

  private subscription: Subscription;
  usuario: UserModel;
  registerDate: string;
  linksGenerated: string;
  linksTotalVisited: string;

  constructor(private user: UserService, private enlaces: EnlacesService, private api: ApiService) {
    this.registerDate = this.statisticRegisterDate();
    this.linksGenerated = this.statisticGenerateEnlaces();
    this.linksTotalVisited = this.statisticTotalVisited();
    this.subscription = this.user.user$.subscribe(usuario => this.usuario = usuario);
    console.log(this.usuario);
  }

  ngAfterViewInit(): void {
    $('.message .close')
      .on('click', function () {
        $(this)
          .closest('.message')
          .transition('fade');
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  click($event: Event): void {
    const cursor = $($event.target);
    // disable button
    cursor.addClass('disabled');
    // remove display node of buttons
    cursor.parent().next().css('display', 'initial');
    // disable read only
    cursor.parent().children().prev().removeAttr('readonly');
  }

  save($event: Event): void {
    const cursor = $($event.target);
    const elemento = cursor.parent().prev().children().prev().attr('name');
    const valor = cursor.parent().prev().children().prev().prop('value');

    const obj = new Object({});
    obj[elemento] = valor;

    this.api.updateUser(obj).subscribe(res => {
      this.user.init(res.data);
    }, err => {
      console.log(err);
    }, () => {
      // disable disabled status
      cursor.parent().prev().children().next().removeClass('disabled');
      // hide buttons
      cursor.parent().css('display', 'none');
      // enable readOnly
      cursor.parent().prev().attr('readonly', '');
    });
  }

  cancel($event: Event): void {
    const cursor = $($event.target);
    // disable disabled status
    cursor.parent().prev().children().next().removeClass('disabled');
    // hide buttons
    cursor.parent().css('display', 'none');
    // enable readOnly
    cursor.parent().prev().attr('readonly', '');
  }

  setPremium(): void {
    this.api.updateUser({ tipoUsuario: UserTypes.Premium }).subscribe(res => {
      this.user.init(res.data);
      alert('¡Eres premium!🎉');
    }, err => {
      console.error(err);
    });
  }

  setFremium(): void {
    this.api.updateUser({ tipoUsuario: UserTypes.Freemium }).subscribe(res => {
      this.user.init(res.data);
      alert('¡Esperemos volver a verte!');
    }, err => {
      console.error(err);
    });
  }

  statisticRegisterDate(): string {
    const id = this.user.user._id;
    const date = this.user.getDateFromObjectId(id);
    return `${date.getDay()}<br>${months[date.getMonth()]}<br>${date.getFullYear()}`;
  }

  statisticGenerateEnlaces(): string {
    let total: number;
    this.enlaces.source$.subscribe(data => total = data.length);
    return total.toString();
  }

  statisticTotalVisited(): string {
    let total = 0;
    this.enlaces.source$.subscribe((enlaces: Card[]) => {
      enlaces.forEach((enlace: Card) => {
        total = total + enlace.metadata.length;
      });
    });
    return total.toString();
  }

  deleteAllEnlaces() {
    this.enlaces.deleteAllEnlaces();
    this.statisticGenerateEnlaces();
    this.statisticTotalVisited();
  }

  deleteAccount() {
    this.api.deleteAccount().subscribe(res => {
      console.log(res);
    }, err => {
      alert(err.data);
    });
  }
}
