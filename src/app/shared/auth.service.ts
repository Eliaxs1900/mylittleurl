import { HttpClient, HttpHeaders } from '@angular/common/http';
import { QueryModel } from './models/query.model';
import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { map } from 'rxjs/operators';
import { TOKEN_NAME, SERVER, PORT } from './global';
import { UserService } from './user.service';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(private http: HttpClient, private user: UserService) {}

  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
  }

  removeToken(): void {
    this.user.removeUser();
  }

  login(user: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    // tslint:disable:object-literal-shorthand
    // tslint:disable:object-literal-key-quotes
    const parametros = new Object({
      'email': user,
      'password': password
    });

    return this.http.post<QueryModel>(
      'http://' + SERVER + ':' + PORT + '/api/usuarios/signin',
      parametros, httpOptions).pipe(
        map(
          res => {
            if ( res.status === 'success') {
              // this.setToken(res.data);
              this.user.init(res.data);
            }
          }
        ));
  }

  register(nombre: string, apellidos: string, email: string, contraseña: string) {
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    // tslint:disable:object-literal-shorthand
    // tslint:disable:object-literal-key-quotes
    const parametros = new Object({
      'nombre': nombre,
      'apellidos': apellidos,
      'email': email,
      'password': contraseña
    });

    return this.http.post<QueryModel>('http://' + SERVER + ':' + PORT + '/api/usuarios/signup',
    parametros, httpOptions).pipe(
      map( res => {
        if (res.status === 'success') {
          this.user.init(res.data);
        }
      }));
  }

  private getTokenExpirationDate(token: string): Date {

    const decoded = jwt_decode<any>(token);
    if (decoded.exp === undefined) { return null; }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) { token = this.getToken(); }
    if (!token) { return true; }

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) { return false; }

    return !(date.valueOf() > new Date().valueOf());
  }
}
