export const SERVER = 'localhost';
export const PORT = '3000';
export const TOKEN_NAME = 'MyLittleToken';
export const IMAGEN_PERFIL = 'imageAvatar';
export const enum screenSize {
    MOBILE = 'mobile',
    TABLET = 'tablet',
    SMALL_MONITOR = 'smallMonitor',
    LARGE_MONITOR = 'largeMonitor'
}

export const enum orderType {
    Title = 'title',
    Time = 'creationDate',
    Impact = 'clicks'
}

export const enum UserTypes {
    Freemium = 'freemium',
    Premium = 'premium',
    Admin = 'admin'
}

export interface Borrar {
    isSelectedToDelete: boolean;
    deleteSelection: boolean;
}
