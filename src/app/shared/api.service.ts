// tslint:disable:object-literal-shorthand
// tslint:disable:object-literal-key-quotes

import { Injectable } from '@angular/core';
import { QueryModel } from './models/query.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UserModel } from './models/user.model';
import { SERVER, PORT, TOKEN_NAME } from './global';
import { Card } from './models/card.model';
import { Router } from '@angular/router';

const API_URL_BASE = `http://${SERVER}:${PORT}/api`;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient, private router: Router) { }

  createEnlace(title: string, link: string) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    const parametros = new Object({
      'descripcion': title,
      'enlace': link
    });

    return this.http.post<QueryModel>(`${API_URL_BASE}/enlaces`, parametros, httpOptions);
  }

  getAllEnlaces() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    return this.http.get<QueryModel>(`${API_URL_BASE}/enlaces`, httpOptions);
  }

  updateEnlace(card: Card) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    const parametros = new Object({
      'enlace': card.link,
      'descripcion': card.title
    });

    return this.http.put<QueryModel>(`${API_URL_BASE}/enlaces/${card.id}`, parametros, httpOptions);
  }

  deleteEnlace(id: string) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    return this.http.delete<QueryModel>(`${API_URL_BASE}/enlaces/${id}`, httpOptions);
  }

  deleteAllEnlaces() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    return this.http.delete<QueryModel>(`${API_URL_BASE}/enlaces`, httpOptions);
  }

  updateUser(object: object) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    const parametros = new Object({
      'descripcion': '',
      'enlace': 'link'
    });

    return this.http.post<QueryModel>(`${API_URL_BASE}/usuarios`, object, httpOptions);
  }

  deleteAccount() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    return this.http.delete<QueryModel>(`${API_URL_BASE}/usuarios`, httpOptions)
      .pipe(map(res => {
        if (res.status === 'success') {
          localStorage.removeItem(TOKEN_NAME);
          this.router.navigate(['/login']);
        }
      }));
  }

  uploadPhoto(file: File) {
    const formData = new FormData();
    formData.append('imageAvatar', file, file.name);
    return this.http.post(`${API_URL_BASE}/usuarios/imagen`, formData);
  }

  getProfilePhoto(): Observable<Blob> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Response-Type': 'blob' as 'json'
      }),
      responseType: 'blob' as 'json'
    };

    return this.http.get<Blob>(`${API_URL_BASE}/usuarios/imagen`, httpOptions);
  }

  setDefaultPhoto(numero: number) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    const parametros = new Object({
      'defaultImage': numero.toString()
    });

    return this.http.post<QueryModel>(`${API_URL_BASE}/usuarios/imagen`, parametros, httpOptions);
  }
}