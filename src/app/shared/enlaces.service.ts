// tslint:disable: variable-name
import { Injectable, OnInit } from '@angular/core';
import { Card } from './models/card.model';
import { ApiService } from './api.service';
import orderBy from 'lodash.orderby';
import { BehaviorSubject } from 'rxjs';
import { orderType } from './global';

@Injectable({
  providedIn: 'root'
})
export class EnlacesService {
  private cards: Card[] = [];
  private sortDescendent;
  private order: orderType;
  private source = new BehaviorSubject<Card[]>(this.cards);
  source$ = this.source.asObservable();

  constructor(private api: ApiService) {
    this.setOrder(orderType.Title);
    this.isSortDescendent(true);

    if (localStorage.getItem('raw-enlaces')) {
      this.loadFromLocal();
    } else {
      this.loadFromServer();
    }
    this.generateCustomArray();
  }

  isSortDescendent(status: boolean): void {
    this.sortDescendent = status ? 'desc' : 'asc';
    this.generateCustomArray();
  }

  setOrder(value: orderType) {
    this.order = value;
    this.generateCustomArray();
  }

  generateCustomArray(): void {
    switch (this.order) {
      case orderType.Impact: {
        this.source.next(orderBy(this.cards, [this.order], [this.sortDescendent]));
        break;
      }

      case orderType.Time: {
        this.source.next(orderBy(this.cards, [this.order], [this.sortDescendent]));
        break;
      }

      case orderType.Title: {
        this.source.next(orderBy(this.cards, [this.order], [this.sortDescendent]));
        break;
      }
    }
  }

  filter(cadena: string): void {
    if (cadena === '') {
      return this.source.next(this.cards);
    }
    // this.source.next(filter(this.cards, card => card.title === cadena));
    // console.log('Filter Array');
    this.source.next(this.cards.filter(item => {
      console.log(item.title);
      console.log(cadena);
      return item.title.includes(cadena) ||
      item.domain().includes(cadena) ||
      item.shortLink.includes(cadena) ||
      item.clicks.toString().includes(cadena);
    }));
  }

  private loadFromLocal(): void {
    const cards = JSON.parse(localStorage.getItem('raw-enlaces'));
    cards.forEach((element: Card) => {
      this.cards.push(new Card(
        element.id,
        element.title,
        element.link,
        element.shortLink,
        element.creationDate,
        element.clicks,
        element.metadata
      ));
    });
    this.source.next(this.cards);
  }

  private loadFromServer(): void {
    this.api.getAllEnlaces().subscribe(
      (res: any) => {
        res.data.forEach(enlace => {
          const metadata = [];
          enlace.visitas.forEach(visita => {
            metadata.push({
              device: visita.device,
              creationDate: visita.creation_date,
              continent: visita.ip_data.continent_name,
              country: visita.ip_data.country_name,
              region: visita.ip_data.region_name
            });
          });

          this.cards.push(new Card(
            enlace._id,
            enlace.descripcion,
            enlace.enlace,
            enlace.shortLink,
            enlace.creation_date,
            enlace.clicks,
            metadata
          ));
        });
        this.source.next(this.cards);
        this.saveOnLocal();
      }, err => {
        console.log('Eror');
      });

  }

  update() {
    console.log('updaate');
    this.cards = [];
    this.loadFromServer();
  }

  deleteAllEnlaces() {
    this.api.deleteAllEnlaces().subscribe(
      res => {
        alert('todos los enlaces han sido borrados');
        this.update();
      }, err => {
        alert('Ha ocurrido un error');
      }
    );
  }

  private saveOnLocal(): void {
    localStorage.setItem('raw-enlaces', JSON.stringify(this.cards));
  }
}
