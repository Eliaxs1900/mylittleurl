import { Injectable } from '@angular/core';
import { UserModel } from './models/user.model';
import * as jwt_decode from 'jwt-decode';
import { TOKEN_NAME, UserTypes } from './global';
import { ApiService } from './api.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: UserModel;
  user$ = new BehaviorSubject<UserModel>(this.user);
  imagen = new BehaviorSubject<string>(this.imagen);
  imagen$ = this.imagen.asObservable();

  constructor(private api: ApiService) {
    const token = localStorage.getItem(TOKEN_NAME);
    if (token) {
      this.init(token);
    }
  }

  init(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
    this.setUser(token);
    this.updateImagen();
  }

  getDateFromObjectId(objectId: string): Date {
    try {
      const timestamp = parseInt(objectId.substr(0, 8), 16) * 1000;
      return new Date(timestamp);
    } catch (error) {
      throw new Error('Invalid ObjectID');
    }
  }

  private setUser(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
    const decoded = jwt_decode(token) as any;
    const usuario = new UserModel(
      decoded.data._id,
      decoded.data.nombre,
      decoded.data.apellidos,
      decoded.data.email,
      decoded.data.password,
      decoded.data.tipoUsuario,
      decoded.data.imagen,
      decoded.data.telefono
    );
    this.user = usuario;
    this.user$.next(this.user);
  }

  removeUser(): void {
    this.user = undefined;
    localStorage.clear();
    sessionStorage.clear();
  }

  isPremium(): boolean {
    return this.user.tipoUsuario !== UserTypes.Freemium;
  }

  private updateImagen() {
    switch (this.user.imagen) {
      case '0': { this.imagen.next('assets/images/boy.svg'); break; }
      case '1': { this.imagen.next('assets/images/dog.svg'); break; }
      case '2': { this.imagen.next('assets/images/girl.svg'); break; }
      case '3': { this.imagen.next('assets/images/cat.svg'); break; }
      default: {
        this.api.getProfilePhoto().subscribe(
          res => {
            this.createImageFromBlob(res);
          },
          error => {
            this.imagen.next('assets/images/falback.jpg');
            console.error('I can\' t download your user profile photo', error);
          });
        break;
      }
    }
  }

  private createImageFromBlob(image: Blob): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.imagen.next(reader.result.toString());
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }
}
