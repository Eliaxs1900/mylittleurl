export class CardsModel {
    id: string;
    description: string;
    link: string;
    totalClicks: number;
    date: string;

    constructor(description: string, link: string, clicks: number, date: string) {
        this.description = description;
        this.link = link;
        this.totalClicks = clicks;
        this.date = date;
    }

    // domain() is a utility function that extracts
    // the domain from a URL, which we'll explain shortly
    domain(): string {
        try {
            // e.g http:foo.com/path/to/bar
            const doaminAndPath: string = this.link.split('//')[1];

            // e.g foo.com/path/to/bar
            return doaminAndPath.split('/')[0];
        } catch (err) {
            return null;
        }
    }
}
