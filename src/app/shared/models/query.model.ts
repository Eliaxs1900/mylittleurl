export class QueryModel {
    // tslint:disable-next-line:variable-name
    public status: string;
    public data: any;
    public extra: string;

    constructor(obj: any) {
        this.status = obj.status;
        this.data = obj.data;
        this.extra = obj.extra;
    }
}
