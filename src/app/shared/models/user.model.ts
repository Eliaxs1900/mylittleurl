import { UserTypes } from '../global';

export class UserModel {
    // tslint:disable-next-line:variable-name
    public _id: string;
    public nombre: string;
    public apellidos: string;
    public email: string;
    public password: string;
    public tipoUsuario: UserTypes;
    public imagen: string;
    public telefono: number;

    constructor(
        // tslint:disable-next-line:variable-name
        _id: string,
        nombre: string,
        apellidos: string,
        email: string,
        password: string,
        tipoUsuario: UserTypes,
        image: string,
        telefono?: number) {
            this._id = _id,
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.email = email;
            this.password = password;
            this.tipoUsuario = tipoUsuario;
            this.imagen = image;
            this.telefono = telefono ? telefono : null;
        }

    toString(): string {
        return (
            `Usuario: _id: ${this._id}, nombre: ${this.nombre}, apellidos: ${this.apellidos},
            email: ${this.email}, password: ${this.password}, tipo_usuario: ${this.tipoUsuario},
            imagen: ${this.imagen}, teléfono: ${this.telefono}`
        );
    }
}
