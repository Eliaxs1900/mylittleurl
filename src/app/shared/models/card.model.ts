interface CardModel {
    id: string;
    title: string;
    link: string;
    shortLink: string;
    creationDate: number;
    clicks: number;
}

export interface CardData {
    device: string;
    creationDate: number;
    continent: string;
    country: string;
    region: string;
}

export class Card implements CardModel {
    id: string;
    title: string;
    link: string;
    shortLink: string;
    creationDate: number;
    clicks: number;
    metadata: CardData[];

    constructor(
        id: string,
        title: string,
        link: string,
        shortLink: string,
        creationDate: number,
        clicks: number,
        metadata: CardData[]
    ) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.shortLink = shortLink;
        this.creationDate = creationDate;
        this.clicks = clicks;
        this.metadata = metadata;
    }

    getMetadatas(): CardData[] {
        return this.metadata;
    }

    addMetadata(data: CardData): void {
        this.metadata.push(data);
    }


    // domain() is a utility function that extracts
    // the domain from a URL, which we'll explain shortly
    domain(): string {
        try {
            // e.g http:foo.com/path/to/bar
            const doaminAndPath: string = this.link.split('//')[1];

            // e.g foo.com/path/to/bar
            return doaminAndPath.split('/')[0];
        } catch (err) {
            return this.link;
        }
    }
}
