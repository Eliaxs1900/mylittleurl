import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { Card } from '../shared/models/card.model';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { EnlacesService } from '../shared/enlaces.service';
import * as Chart from 'chart.js';
import * as uniq from 'lodash.uniq';
import { ApiService } from '../shared/api.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'enlace-info',
  templateUrl: './show-enlace.component.html',
  styleUrls: ['./show-enlace.component.css']
})
export class ShowDataEnlaceComponent implements OnInit, AfterContentChecked {
  card: Card;
  isModificated: boolean;
  isLoading: boolean;
  chart: any;
  constructor(private api: ApiService, private route: ActivatedRoute, private router: Router, private enlacesService: EnlacesService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const id = +params.get('id');
      this.enlacesService.source$.subscribe((cards: Card[]) => {
        this.card = cards[id - 1];
      });
    });

    this.generarDatosDispositivos();
    this.generarDatosContinent();
    this.generarDatosCountry();
    this.generarDatosRegion();
  }

  ngAfterContentChecked(): void {
    $('.ui.modal')
      .modal({
        context: $('#MoreInfo'),
        autofocus: false,
        onApprove: () => {
          if (this.isModificated) {
            this.api.updateEnlace(this.card).subscribe(response => {
              if (response.status === 'sucess') {
                this.enlacesService.update();
              }
            });
          }
        },
        onHide: () => {
          this.router.navigate(['/main/enlaces']);
        }
      })
      .modal('show');
  }

  copiar() {
    ($('#shortLink') as unknown as HTMLInputElement).select();
    document.execCommand('copy');
  }

  editar() {
    this.isModificated = true;
    $('.ui.positive.button')[0].childNodes[0].nodeValue = 'Guardar';
    $('input')[1].removeAttribute('readonly');
    $('input')[2].removeAttribute('readonly');
  }

  borrar() {
    this.api.deleteEnlace(this.card.id).subscribe(response => {
      if (response.status === 'success') {
        this.enlacesService.update();
        $('.ui.modal').modal('hide');
      }
    });
  }

  generarDatosDispositivos() {
    const labelsDispositivos = [];
    const numberDispositivos = [];
    const coloresRandom = [];

    const devices = [];
    this.card.metadata.forEach(metadata => {
      if (metadata.device !== null) {
        devices.push(metadata.device.toString());
      } else {
        devices.push('unknow');
      }
    });

    if (devices.length === 0) {
      devices.push('Nothing');
    }

    uniq(devices).forEach(device => labelsDispositivos.push(device));
    let contador = 0;
    labelsDispositivos.forEach(device => {
      devices.forEach(device1 => {
        if (device1 === device) {
          contador += 1;
        }
      });
      numberDispositivos.push(contador);
      contador = 0;
    });

    numberDispositivos.forEach(() => coloresRandom.push(this.getRandomColor()));

    this.chart = new Chart('pie1', {
      type: 'pie',
      data: {
        labels: labelsDispositivos,
        datasets: [{
          data: numberDispositivos,
          backgroundColor: coloresRandom
        }]
      },
      options: {
        legend: { display: false },
        scales: {
          xAxes: [{ display: false }],
          yAxes: [{ display: false }]
        }
      }
    });
  }

  generarDatosContinent() {
    const labelsContinents = [];
    const numberContinents = [];
    const coloresRandom = [];

    const continents = [];
    this.card.metadata.forEach(metadata => {
      if (metadata.continent !== null) {
        continents.push(metadata.continent.toString());
      } else {
        continents.push('unknow');
      }
    });

    if (continents.length === 0) {
      continents.push('Nothing');
    }

    uniq(continents).forEach(continent => labelsContinents.push(continent));
    let contador = 0;
    labelsContinents.forEach(continent => {
      continents.forEach(continent1 => {
        if (continent1 === continent) {
          contador += 1;
        }
      });
      numberContinents.push(contador);
      contador = 0;
    });

    numberContinents.forEach(() => coloresRandom.push(this.getRandomColor()));

    this.chart = new Chart('pie2', {
      type: 'pie',
      data: {
        labels: labelsContinents,
        datasets: [{
          data: numberContinents,
          backgroundColor: coloresRandom
        }]
      },
      options: {
        legend: { display: false },
        scales: {
          xAxes: [{ display: false }],
          yAxes: [{ display: false }]
        }
      }
    });
  }

  generarDatosCountry() {
    const labelsCountries = [];
    const numberCountries = [];
    const coloresRandom = [];

    const countries = [];
    this.card.metadata.forEach(metadata => {
      if (metadata.country !== null) {
        countries.push(metadata.country.toString());
      } else {
        countries.push('unknow');
      }
    });

    if (countries.length === 0) {
      countries.push('Nothing');
    }

    uniq(countries).forEach(country => labelsCountries.push(country));
    let contador = 0;
    labelsCountries.forEach(country => {
      countries.forEach(country1 => {
        if (country1 === country) {
          contador += 1;
        }
      });
      numberCountries.push(contador);
      contador = 0;
    });

    numberCountries.forEach(() => coloresRandom.push(this.getRandomColor()));

    this.chart = new Chart('pie3', {
      type: 'pie',
      data: {
        labels: labelsCountries,
        datasets: [{
          data: numberCountries,
          backgroundColor: coloresRandom
        }]
      },
      options: {
        legend: { display: false },
        scales: {
          xAxes: [{ display: false }],
          yAxes: [{ display: false }]
        }
      }
    });
  }

  generarDatosRegion() {
    const labelsRegions = [];
    const numberRegions = [];
    const coloresRandom = [];

    const regions = [];
    this.card.metadata.forEach(metadata => {
      if (metadata.region !== null) {
        regions.push(metadata.region.toString());
      } else {
        regions.push('unknow');
      }
    });

    if (regions.length === 0) {
      regions.push('Nothing');
    }

    uniq(regions).forEach(region => labelsRegions.push(region));
    let contador = 0;
    labelsRegions.forEach(region => {
      regions.forEach(region1 => {
        if (region1 === region) {
          contador += 1;
        }
      });
      numberRegions.push(contador);
      contador = 0;
    });

    numberRegions.forEach(() => coloresRandom.push(this.getRandomColor()));

    this.chart = new Chart('pie4', {
      type: 'pie',
      data: {
        labels: labelsRegions,
        datasets: [{
          data: numberRegions,
          backgroundColor: coloresRandom
        }]
      },
      options: {
        legend: { display: false },
        scales: {
          xAxes: [{ display: false }],
          yAxes: [{ display: false }]
        }
      }
    });
  }

  getRandomColor() {
    const colorArray = [
      '#F9DC5C', '#403F4C', '#E84855', '#3185FC', '#00B3E6',
      '#A5DFDA', '#7BCEC6', '#57B4AB', '#27A397', '#04897C',
      '#FF4646', '#CE3F3C', '#9E3832', '#6E3229', '#3D2B1F',
      '#009F6B', '#25453A', '#3D8B96', '#89C1A9', '#465E3C',
      '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
      '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
      '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
      '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF',
      '#F9DC5C', '#403F4C', '#E84855', '#3185FC', '#00B3E6',
      '#A5DFDA', '#7BCEC6', '#57B4AB', '#27A397', '#04897C',
      '#FF4646', '#CE3F3C', '#9E3832', '#6E3229', '#3D2B1F',
      '#009F6B', '#25453A', '#3D8B96', '#89C1A9', '#465E3C',
      '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
      '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
      '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
      '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];
    return colorArray[Math.floor(Math.random() * colorArray.length)];
  }

}
