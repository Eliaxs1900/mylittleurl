
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Card, CardData } from '../shared/models/card.model';
import { EnlacesService } from '../shared/enlaces.service';
import { Subscription, Observable } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'enlaces',
  templateUrl: './enlaces.component.html',
  styleUrls: ['./enlaces.component.css']
})
export class EnlacesComponent implements OnInit, OnDestroy {
  sortDescendent = false;
  subscription: Subscription;
  cards: Card[];

  constructor(private enlacesService: EnlacesService) {}

  ngOnInit(): void {
    this.subscription =  this.enlacesService.source$.subscribe(
      (cards) => this.cards = cards
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
