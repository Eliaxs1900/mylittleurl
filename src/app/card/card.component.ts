import { Component, Input, AfterViewInit, HostBinding, } from '@angular/core';
import { Chart } from 'chart.js';
import { Card } from '../shared/models/card.model';
import { Borrar } from '../shared/global';

export class Dispositivo {
  tipo: string;
  cantidad: number;

  constructor(tipo: string, cantidad: number) {
    this.tipo = tipo;
    this.cantidad = cantidad;
  }
}

export class PuntoTemporal {
  dia: number;
  visitas: number;

  constructor(dia: number, visitas: number) {
    this.dia = dia;
    this.visitas = visitas;
  }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements AfterViewInit, Borrar {
  isSelectedToDelete: boolean;
  deleteSelection: boolean;
  @HostBinding('class.ui')
  @HostBinding('class.link')
  @HostBinding('class.card')
  @Input() card: Card;
  @Input() id: number;
  chartTime: any;
  total: number;

  constructor() {
    this.total = 0;
    this.isSelectedToDelete = true;
  }

  ngAfterViewInit(): void {
    // ⚡🔥 setTimeOut() evita que angular lance el error
    // "Expression has changed after it was checked"
    // More info: https://blog.angular-university.io/angular-debugging/
    setTimeout(() => { this.generarDatosTiempo(); });
  }

  generarDatosTiempo() {
    const puntos: PuntoTemporal[] = [
      new PuntoTemporal(1, 43),
      new PuntoTemporal(4, 50),
      new PuntoTemporal(13, 73),
      new PuntoTemporal(20, 12),
      new PuntoTemporal(21, 83),
      new PuntoTemporal(63, 43),
      new PuntoTemporal(65, 50),
      new PuntoTemporal(70, 73),
      new PuntoTemporal(73, 12),
      new PuntoTemporal(81, 83),
      new PuntoTemporal(83, 43)
    ];

    const labelsDias = [];
    const numberVisitas = [];
    const coloresRandom = [];
    puntos.forEach((punto) => labelsDias.push(punto.dia));
    puntos.forEach((punto) => numberVisitas.push(punto.visitas));
    puntos.forEach(() => coloresRandom.push(this.getRandomColor()));

    const ctx = $(`#line${this.id}`)[0] as HTMLCanvasElement;
    this.chartTime = new Chart(ctx, {
      type: 'line',
      data: {
        labels: labelsDias,
        datasets: [{
          data: numberVisitas,
          // backgroundColor: coloresRandom,
          fill: false
        }]
      },
      options: {
        legend: { display: false },
        scales: {
          xAxes: [{ display: false }],
          yAxes: [{ display: false }]
        },
        title: {
          display: true,
          text: '',
          position: 'bottom'
        }
      }
    });
  }

  getRandomColor() {
    const colorArray = [
      '#F9DC5C', '#403F4C', '#E84855', '#3185FC', '#00B3E6',
      '#A5DFDA', '#7BCEC6', '#57B4AB', '#27A397', '#04897C',
      '#FF4646', '#CE3F3C', '#9E3832', '#6E3229', '#3D2B1F',
      '#009F6B', '#25453A', '#3D8B96', '#89C1A9', '#465E3C',
      '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
      '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
      '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
      '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF',
      '#F9DC5C', '#403F4C', '#E84855', '#3185FC', '#00B3E6',
      '#A5DFDA', '#7BCEC6', '#57B4AB', '#27A397', '#04897C',
      '#FF4646', '#CE3F3C', '#9E3832', '#6E3229', '#3D2B1F',
      '#009F6B', '#25453A', '#3D8B96', '#89C1A9', '#465E3C',
      '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
      '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
      '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
      '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];
    return colorArray[Math.floor(Math.random() * colorArray.length)];
  }
}
