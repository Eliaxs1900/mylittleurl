import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { QueryModel } from '../shared/models/query.model';
import { TOKEN_NAME } from '../shared/global';
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<QueryModel>> {
        return next.handle(request).pipe(
            catchError(err => {
                // Print Err in the console DEV ONLY
                console.error(err.message);
                if (err.status === 401) {
                    // auto logout if 401 response returned from api
                    localStorage.removeItem(TOKEN_NAME);
                    this.router.navigate(['/login']);
                }

                if (err.status === 0) {
                    // No server response or no connection
                    // tslint:disable-next-line:no-shadowed-variable
                    const respe = new QueryModel(new Object());
                    respe.status = err.ok;
                    respe.data = 'Can\'t connect with server, check your connection';
                    respe.extra = err.message;
                    return throwError(respe);
                }

                const respe = new QueryModel(new Object());
                respe.status = err.ok;
                respe.data = err.error.data;
                respe.extra = err.error.extra;
                return throwError(respe);
            }));
    }
}
