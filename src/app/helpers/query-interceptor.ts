import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { TOKEN_NAME } from '../shared/global';

@Injectable()
export class QueryInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem(TOKEN_NAME);
    if (token) {
      req = req.clone({
        setHeaders: { Authorization: token }
      });
    }

    return next.handle(req);
  }
}
