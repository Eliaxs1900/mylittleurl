import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { SignupFormComponent } from './login/signup-form/signup-form.component';
import { ErrorInterceptor } from './helpers/error-interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { QueryInterceptor } from './helpers/query-interceptor';
import { UploadAvatarComponent } from './upload-avatar/upload-avatar.component';
import { EnlacesComponent } from './enlaces/enlaces.component';
import { ConfigPageComponent } from './config-page/config-page.component';
import { CardComponent } from './card/card.component';
import { NewEnlaceComponent } from './new-enlace/new-enlace.component';
import { ShowDataEnlaceComponent } from './show-enlace/show-enlace.component';
import { UserService } from './shared/user.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginFormComponent,
    SignupFormComponent,
    MainComponent,
    UploadAvatarComponent,
    EnlacesComponent,
    ConfigPageComponent,
    CardComponent,
    NewEnlaceComponent,
    ShowDataEnlaceComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, // <- SUPER IMPORTANTE
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: QueryInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
