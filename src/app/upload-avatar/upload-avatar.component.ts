import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/shared/api.service';
import { TOKEN_NAME } from 'src/app/shared/global';
import { UserService } from 'src/app/shared/user.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'subir-imagen',
  templateUrl: './upload-avatar.component.html',
  styleUrls: ['./upload-avatar.component.css']
})

export class UploadAvatarComponent implements OnInit {
  image: any;
  @Output() newImage = new EventEmitter();
  @Output() cerrar = new EventEmitter<string>();
  progressBar: HTMLElement;

  constructor(private api: ApiService, private user: UserService) {
    this.user.imagen$.subscribe((imagen) => {
      this.image = imagen;
    });
  }

  ngOnInit(): void {
    $('#ImagenModal')
    .modal({
      onDeny: () => {
      },
      onApprove: () => {
      },
      onHide: () => {
        this.cerrar.emit();
      }
    })
    .modal('show');
    $('.image.content .dimmable.image').dimmer({ on: 'hover' });
    this.progressBar = document.getElementById('progressBar') as HTMLElement;
  }

  // Upload personal photo
  uploadImage(event) {
    const reader = new FileReader();

    // Lectura del fichero
    reader.onloadstart = () => {
      console.log('onloadStart');
      this.progressBar.style.transition = '100ms';
      this.progressBar.style.opacity = '1';
    };

    reader.onloadend = () => {
      console.log('onloadend');
    };


    reader.readAsDataURL(event.target.files[0]);
    reader.addEventListener('load', () => {
      console.log('Load');
    });

    reader.onprogress = (data) => {
      if (data.lengthComputable) {
        this.setPercent(parseInt(((data.loaded / data.total) * 100).toString(), 10));
        console.log(parseInt(((data.loaded / data.total) * 100).toString(), 10));
      }
    };

    reader.onload = () => {
      this.api.uploadPhoto(event.target.files[0]).subscribe(
        (res: any) => {
          localStorage.setItem(TOKEN_NAME, res.data);
          this.user.imagen.next(reader.result);

          this.progressBar.style.transition = '500ms';
          setInterval(() => { this.progressBar.style.opacity = '0'; }, 500);
        }, err => {
          console.log(err);
        }
      );
      this.setPercent(100);
    };
  }

  setPercent(percent: number) {
    $('.ui.teal.progress').progress('set percent', percent);
  }


  // When user click in a default user avatar
  setDefaultAvatar(numero: number): void {
    this.api.setDefaultPhoto(numero).subscribe(
      res => {
        localStorage.setItem(TOKEN_NAME, res.data);
        this.user.init(res.data);
      }, err => {
        alert('error');
      }
    );
  }

}
