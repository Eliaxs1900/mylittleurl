import { Component, OnInit, HostListener } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  showLoginForm: boolean;

  constructor(private auth: AuthService, private router: Router) {
    this.showLoginForm = true;
  }

  ngOnInit(): void {
    console.log(this.auth.isTokenExpired());
    if (!this.auth.isTokenExpired()) {
      this.router.navigate(['/']);
    }
  }
}
