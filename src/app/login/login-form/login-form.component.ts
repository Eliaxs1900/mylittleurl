import { Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/user.service';
import { EnlacesService } from 'src/app/shared/enlaces.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent {
  email: string; pass: string;

  isloading: boolean;
  errorEmail: boolean;
  errorPass: boolean;
  uncknowError: string;

  constructor(private authService: AuthService, private user: UserService, private enlaces: EnlacesService, private router: Router) {}

  login(): void {
    this.isloading = true;

    this.authService.login(this.email, this.pass).subscribe(
      () => {
        this.isloading = false;
        this.enlaces.update();
        this.router.navigate(['']);
      },
      err => {
        this.isloading = false;
        if (err.data === 'Email was not found') {
          this.errorEmail = true;
        } else if (err.data === 'Wrong password') {
          this.errorPass = true;
        } else {
          this.uncknowError = err.data;
        }
      }
    );
  }
}
