import { Component } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { Router } from '@angular/router';
import { EnlacesService } from 'src/app/shared/enlaces.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent {
  nombre: string;
  apellidos: string;
  email: string;
  password: string;
  showPassword: boolean;

  isloading: boolean;
  errorEmail: boolean;
  uncknowError: string;

  constructor(private authService: AuthService, private enlaces: EnlacesService, private router: Router) {}

  signup(): void {
    this.isloading = !this.isloading;

    this.authService.register(this.nombre, this.apellidos, this.email, this.password).subscribe(
      () => {
        this.isloading = false;
        this.enlaces.update();
        this.router.navigate(['']);
      }, err => {
        this.isloading = false;
        if (err.data === 'This email has already used') {
          this.errorEmail = true;
        } else {
          this.uncknowError = err.data;
        }
      }
    );
  }

  switchShowPassword(): void {
    this.showPassword = !this.showPassword;
  }

}
